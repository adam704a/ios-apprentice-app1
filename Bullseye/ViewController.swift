//
//  ViewController.swift
//  Bullseye
//
//  Created by Adam Preston on 5/6/15.
//  Copyright (c) 2015 Adam Preston. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    var currentValue = 25
    var targetValue = 0
    var score = 0
    var round = 0
    
    func startNewRound() {
        round += 1
        targetValue = 1 + Int(arc4random_uniform(100))
        currentValue = 50
        slider.value = Float(currentValue)
    }
    
    func updateLabels() {
        
        scoreLabel.text = String(score)
        targetLabel.text = String(targetValue)
        roundLabel.text = String(round)
    }
    
    
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var targetLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var roundLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        startNewGame()
        updateLabels()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func startNewGame() {
        score = 0
        round = 0
        startNewRound()
    }
    
    @IBAction func startOver() {
        startNewGame()
        updateLabels()
    }
    
    @IBAction func showAlert() {
    
        let difference = abs(targetValue - currentValue)
        let points = 100 - difference
        score += points
        var title: String
        
        if difference == 0 {
            title = "Perfect!"
        } else if difference < 5 {
            title = "You almost had it!"
        } else if difference < 10 {
            title = "Holy shit, you're terrible"
        } else{
            title = "I cannot believe how fucking bad you are at this."
        }
        
        let message = "You scored \(points) points"
        
        
        
        let alert = UIAlertController(title: title,
            message: message, preferredStyle: .Alert)
        
        let action = UIAlertAction(title: "OK", style: .Default,
            handler: {
                action in
                self.startNewRound()
                self.updateLabels()
            })
        
        alert.addAction(action)
        
        presentViewController(alert, animated: true, completion: nil)

    }
    
    @IBAction func sliderMoved(slider: UISlider) {
            println("The value of the slider is now: \(slider.value)")
            currentValue = lroundf(slider.value)
    }
    
    
    
}

